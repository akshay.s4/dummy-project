const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const User = require("./db/User");

const app = express();
app.use(cors());
app.use(express.json());

mongoose
  .connect(
    "mongodb+srv://admin:admin@cluster0.4cu1rfq.mongodb.net/demo?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  .then((res) => {
    console.log("CONNECTED TO DATABASE!");
  });

app.post("/register", async (req, res) => {
  const { name, email, password } = req.body;

  try {
    const newUser = new User({
      name: name,
      email: email,
      password: password,
    });

    await newUser.save();
    res.status(200).json({
      status: "success",
      data: newUser,
    });
  } catch (err) {
    console.log(err);
  }
});

app.listen(4000);
