import React from 'react'
import About from './Header/About'
import Build from './Header/Build'
import BuildCard from './Header/BuildCard'
import Grow from './Header/Grow'
import GrowCard from './Header/GrowCard'
import Manage from './Header/Manage'
import ManageCard from './Header/ManageCard'
import Review1 from './Header/Review1'
import Review2 from './Header/Review2'
import Review3 from './Header/Review3'
import Subscription from './Header/Subscription'
import Support from './Header/Support'
import SupportCard from './Header/SupportCard'




export default function FeaturesPage() {
    return (
        <>
            <About />
            <Build />
            <BuildCard />
            <Review1 />
            <Grow />
            <GrowCard />
            <Review2 />
            <Manage />
            <ManageCard />
            <Review3 />
            <Support />
            <SupportCard />
            <Subscription />
        </>
    )
}
