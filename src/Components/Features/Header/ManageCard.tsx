import * as React from 'react';

interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    return (
        <div className="features-details">
            <div className="features-Details">
                <div className="card4">
                    <div className="card4-col">

                        <h3 className="fontFamily-Reckless text-m">Manage taxes with ease</h3>
                        <p className="fontFamily-Basis text-reg">Do business anywhere without having to worry about tax headaches. Whether it’s remittance or tax forms, we’ll handle taxes so you don’t have to collect manually.</p>

                    </div>
                    <div className="card4-col">

                        <h3 className="fontFamily-Reckless text-m">Gain powerful insights</h3>
                        <p className="fontFamily-Basis text-reg">Understand your business performance and proactively adjust strategies using our advanced data reporting. We provide intuitive reporting on sales and student engagement to help you make data-informed decisions.</p>

                    </div>
                    <div className="card4-col">

                        <h3 className="fontFamily-Reckless text-m">Stay protected</h3>
                        <p className="fontFamily-Basis text-reg">We offer industry-leading fraud protection, encryption, and backup capabilities so you stay fully protected and in control of your business and data. Plus, with an uptime of 99.99%, we ensure outages won’t knock you out.</p>

                    </div>
                    <div className="card4-col">

                        <h3 className="fontFamily-Reckless text-m">Build better relationships</h3>
                        <p className="fontFamily-Basis text-reg">Business management features such as automatic payouts and custom user roles make collaboration easy, while student features, such as intuitive comments management, help you connect with your customers.</p>

                    </div>
                </div>
            </div>
        </div>
    )

};

export default App;
