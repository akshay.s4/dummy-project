import * as React from 'react';

interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    return (
        <div className="features-details">
            <div className="features-Details">
                <div className="card4">
                    <div className="card4-col">

                        <h3 className="fontFamily-Reckless text-m">Get personalized guidance</h3>
                        <p className="fontFamily-Basis text-reg">From helping you to set up your school to mapping out your launch strategy, our dedicated customer success team is here to help. They host live group coaching sessions three times a week, every week.</p>

                    </div>
                    <div className="card4-col">

                        <h3 className="fontFamily-Reckless text-m">Join the community</h3>
                        <p className="fontFamily-Basis text-reg">Our exclusive member community, <a href="https://teachable.com/community" style={{ color: "blue" }}>teachable:hq,</a> lets you connect with peers, build a valuable network, and get exclusive content to help you grow.</p>

                    </div>
                    <div className="card4-col">

                        <h3 className="fontFamily-Reckless text-m">Learn from the best</h3>
                        <p className="fontFamily-Basis text-reg">Get best practices and insider know-how through our exclusive programming which includes free live events, the Teachable blog, and our flagship training program, teachable:u. Plus, our award-winning customer support team is here to help with any issues you may have, whether through email or live chat.</p>

                    </div>
                    <div className="card4-col">

                        <h3 className="fontFamily-Reckless text-m">Work with experts</h3>
                        <p className="fontFamily-Basis text-reg">Take your business to the next level with help from <a href="https://teachable.com/community" style={{ color: "blue" }}>experienced professionals,</a> exclusive to Teachable, in everything from marketing to web design to course-building.</p>

                    </div>
                </div>
            </div>
        </div>
    )

};

export default App;
