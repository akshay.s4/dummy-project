import * as React from 'react';
import Button from 'react-bootstrap/Button';
import Support from '../Images/Support.jpg'
interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    return (
        <div className="BuildContainer" >
            <div className="BuildInner">
                <div className="AboutText">
                    <h1 className="text-xxl fontFamily-Basis">Support</h1>
                    <p className="text-reg fontFamily-Basis">No matter where you are on your business journey, we get you the support you need, every step of the way.</p>

                    <div className="mb-3 " >
                        <Button variant="secondary" style={{ background: "#21CD9C", color: "black" }} size="lg">
                            Start for free
                        </Button>
                    </div>
                </div>
            </div>

            <div className="grid-item">
                <img src={Support} alt="img" className="img-B" />
            </div>

        </div>
    )

};

export default App;
