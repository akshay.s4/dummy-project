import * as React from 'react';
import Button from 'react-bootstrap/Button';
import Build from '../Images/Build.jpg'
interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    return (
        <div className="BuildContainer" >
            <div className="BuildInner">
                <div className="AboutText">
                    <h1 className="text-xxl fontFamily-Basis">Build</h1>
                    <p className="text-reg fontFamily-Basis">Bring your business online with the features you need to build a great product.</p>

                    <div className="mb-3 " >
                        <Button variant="secondary" style={{ background: "#21CD9C", color: "black" }} size="lg">
                            Start for free
                        </Button>
                    </div>
                </div>
            </div>

            <div className="grid-item">
                <img src={Build} alt="img" className="img-B" />
            </div>

        </div>
    )

};

export default App;
