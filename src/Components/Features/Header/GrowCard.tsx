import * as React from 'react';

interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    return (
        <div className="features-details">
            <div className="features-Details">
                <div className="card4">
                    <div className="card4-col">

                        <h3 className="fontFamily-Reckless text-m">Monetize in more ways</h3>
                        <p className="fontFamily-Basis text-reg">Get paid by customers from anywhere in the world. We accept payments in over 130 currencies via Apple Pay, Google Pay, PayPal, and more. Additionally, you can set up free trials, subscription payments, installments, and more for added flexibility.</p>

                    </div>
                    <div className="card4-col">

                        <h3 className="fontFamily-Reckless text-m">Boost your earning potential</h3>
                        <p className="fontFamily-Basis text-reg">With order bumps, bundles, memberships, upsells, and coupons, you’ll have more ways to sell than ever before.</p>

                    </div>
                    <div className="card4-col">

                        <h3 className="fontFamily-Reckless text-m">Extend the power of the platform</h3>
                        <p className="fontFamily-Basis text-reg">Use affiliate marketing tools to recruit partners to help promote your business on Teachable. Similarly, with our student referrals features, you can incentivize existing customers to refer their friends to your course.</p>

                    </div>
                    <div className="card4-col">

                        <h3 className="fontFamily-Reckless text-m">Scale on your terms</h3>
                        <p className="fontFamily-Basis text-reg">Create any number of courses you want. Repurpose content and lectures with ease so you can scale even faster. Plus, our bulk upload feature lets you seamlessly migrate student information and courses from another platform.</p>

                    </div>
                </div>
            </div>
        </div>
    )

};

export default App;
