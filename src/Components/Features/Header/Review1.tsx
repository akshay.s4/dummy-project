import * as React from 'react';

interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    return (
        <div className="Review">
            <div className="ReviewContainer">
                <h1 className="fontFamily-Reckless text-m ">
                    “Teachable made it simple to create and sell a great-looking course that started generating income immediately. What an amazing thing to be making a living doing what I've been doing all along - sharing what I know!”</h1>
                <p className="fontFamily-Basis text-s ">Marc Sabatella, Mastering MuseScore School</p>
            </div>
        </div>
    )

};

export default App;
