import * as React from 'react';

interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    return (
        <div className="features-details">
            <div className="features-Details">
                <div className="card4">
                    <div className="card4-col">

                        <h3 className="fontFamily-Reckless text-m">Create with ease</h3>
                        <p className="fontFamily-Basis text-reg">Whether it’s self-paced courses, cohort-based courses, or live coaching, you can do it all on Teachable. Start creating your course or coaching service quickly with our intuitive drag-and-drop builder.</p>

                    </div>
                    <div className="card4-col">

                        <h3 className="fontFamily-Reckless text-m">Start selling in minutes</h3>
                        <p className="fontFamily-Basis text-reg">Start selling right away with our built-in, fully customizable sales page builder. Then, build a checkout page with full e-commerce capabilities so you can get paid quickly—no need for a third-party payment system.</p>

                    </div>
                    <div className="card4-col">

                        <h3 className="fontFamily-Reckless text-m">Extend the power of the platform</h3>
                        <p className="fontFamily-Basis text-reg">Connect with the tools you already use and love. We offer direct integrations with MailChimp, Zapier, ConvertKit, Google Analytics, Segment, and more. Plus, use our public API to automate your workflow even more.</p>

                    </div>
                    <div className="card4-col">

                        <h3 className="fontFamily-Reckless text-m">Optimize student engagement</h3>
                        <p className="fontFamily-Basis text-reg">Use bespoke student-side features like comments, quizzes, and certifications of completion to drive learning outcomes and student satisfaction.</p>

                    </div>
                </div>
            </div>
        </div>
    )

};

export default App;
