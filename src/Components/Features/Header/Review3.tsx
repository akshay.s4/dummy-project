import * as React from 'react';

interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    return (
        <div className="Review">
            <div className="ReviewContainer3">
                <h1 className="fontFamily-Reckless text-m ">
                    “I came from Udemy 5 years ago, looking to host my own school with my own rules. I wanted to focus on courses and teachable provided a fully-featured solution, saving me months/years of development work. I really love the peace of mind Teachable is providing me: I can focus on my courses and students knowing that they have my back on the technical end.”</h1>
                <p className="fontFamily-Basis text-s ">Maxime Britto, Purple Giraffe</p>
            </div>
        </div>
    )

};

export default App;
