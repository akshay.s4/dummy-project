import * as React from 'react';

interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    return (
        <div className="Review">
            <div className="ReviewContainer2">
                <h1 className="fontFamily-Reckless text-m ">
                    “Teachable provides me the tools, support, and strength to see my dreams happen. Not only does Teachable boost my income, but it also boosts my confidence”</h1>
                <p className="fontFamily-Basis text-s ">Charles Clifford Brooks III, the Working Writer</p>
            </div>
        </div>
    )

};

export default App;
