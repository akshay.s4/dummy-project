import * as React from 'react';
import Grow from '../Images/Grow.jpg'
import Button from 'react-bootstrap/Button';
interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    return (
        <div className="BuildContainer" >
            <div className="BuildInner">
                <div className="AboutText">
                    <h1 className="text-xxl fontFamily-Basis">Grow</h1>
                    <p className="text-reg fontFamily-Basis">Take your business to greater heights—whether that’s through growing your audience or finding new ways to generate revenue.</p>

                    <div className="mb-3 " >
                        <Button variant="secondary" style={{ background: "#21CD9C", color: "black" }} size="lg">
                            Start for free
                        </Button>
                    </div>
                </div>
            </div>

            <div className="grid-item">
                <img src={Grow} alt="img" className="img-B" />
            </div>

        </div>
    )

};

export default App;
