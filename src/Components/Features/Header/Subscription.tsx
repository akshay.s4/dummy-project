import * as React from 'react';
import Button from 'react-bootstrap/Button';
interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    return (
        <div className="subs-container">
            <div className="subs-content">
                <h2 className="fontFamily-Reckless text-xl">Turn your knowledge into a profitable online business with Teachable</h2>
            </div>
            <div className="mb-3 subs-btn" >
                <Button variant="secondary" style={{ background: "Black" }} size="lg">
                    Get started
                </Button>
            </div>
        </div>
    );
};

export default App;