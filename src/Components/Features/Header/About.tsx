import * as React from 'react';
import Button from 'react-bootstrap/Button';
import About from '../Images/About.jpg'

interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    return (
        <div className="AboutContainer">
            <div className="AboutInner">
                <div className="AboutText">
                    <h1 className="text-xxl fontFamily-Reckless">Your platform, your partners</h1>
                    <p className="text-reg fontFamily-Basis">Grow a thriving online course business with powerful support features including live coaching, in-depth training, and more. </p>
                </div>
                <div className="mb-3 " >
                    <Button variant="secondary" style={{ background: "#21CD9C", color: "black" }} size="lg">
                        Get started
                    </Button>
                </div>
            </div>

            <div className="grid-item">
                <img src={About} alt="img" className="img-A" />
            </div>

        </div>
    )

};

export default App;
