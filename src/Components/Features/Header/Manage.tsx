import * as React from 'react';
import Button from 'react-bootstrap/Button';
import Manage from '../Images/Manage.jpg'
interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    return (
        <div className="BuildContainer" >
            <div className="BuildInner">
                <div className="AboutText">
                    <h1 className="text-xxl fontFamily-Basis">Manage</h1>
                    <p className="text-reg fontFamily-Basis">Whether it’s compliance, taxes, or business management, we automate all the tedious tasks for your business so you can focus on what you do best.</p>

                    <div className="mb-3 " >
                        <Button variant="secondary" style={{ background: "#21CD9C", color: "black" }} size="lg">
                            Start for free
                        </Button>
                    </div>
                </div>
            </div>

            <div className="grid-item">
                <img src={Manage} alt="img" className="img-B" />
            </div>

        </div>
    )

};

export default App;
