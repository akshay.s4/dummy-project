import React from 'react'
import About from './Header/About';
import AboutVideo from './Header/AboutVideo'
import Contact from './Header/Contact'
import Elaborate from './Header/Elaborate'
import Features from './Header/Features'
import FeaturesDetails from './Header/FeaturesDetails'
import Register from './Header/Register'
import Review from './Header/Review'
import SignUp from './Header/Signup'

export default function Main() {
    return (
        <>
            <About />
            <Elaborate />
            <AboutVideo />
            <Features />
            <FeaturesDetails />
            <Review />
            <Register />
            <Contact />
            <SignUp />
        </>
    )
}
