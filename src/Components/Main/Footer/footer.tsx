import * as React from 'react';

interface IFooterProps {
}

const Footer: React.FunctionComponent<IFooterProps> = (props) => {
    return (
        <div className="main-footer">
            <div className="footer-container">
                <div className="rowss">
                    <div className="cols ">
                        <h2 className="text-l">teach:able</h2>
                        <ul className="list-unstyled">
                            <li className="fontFamily-Basis text-reg"> Join the more than 100,000 creators who use Teachable to share their knowledge. Easily create an online course or coaching business with our powerful yet simple all-in-one platform.</li>
                        </ul>
                    </div>
                    <div className="col-links">
                        <div className="col">
                            <h4>Explore</h4>
                            <ul className="list-unstyled">
                                <li>Features</li>
                                <li>Pricing</li>
                                <li>Examples</li>
                                <li>Newsletter</li>
                                <li>Community</li>
                                <li>Podcast</li>
                            </ul>
                        </div>
                        <div className="col">
                            <h4>Company</h4>
                            <ul className="list-unstyled">
                                <li>About</li>
                                <li>Career</li>
                                <li>Blog</li>
                                <li>Press</li>
                                <li>Partners</li>
                                <li>Discover</li>
                            </ul>
                        </div>
                        <div className="col">
                            <h4>Support</h4>
                            <ul className="list-unstyled">
                                <li>Knowledge Base</li>
                                <li>Contact Support</li>
                                <li>Contact Sales</li>
                                <li>Privacy Policy</li>
                                <li>Terms of Use</li>
                                <li>Content Guideliness</li>
                                <li>House Rules</li>
                                <li>Cookies Policy</li>
                                <li>Ethics Line</li>
                                <li>Accessibility</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <hr />
                <div className="row">
                    <p className="col-sm">
                        Copyright &copy; {new Date().getFullYear()} Teachable, Inc. <br />All rights reserved.
                    </p>
                </div>
            </div>
        </div>
    );
};

export default Footer;
