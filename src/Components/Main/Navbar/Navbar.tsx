import * as React from 'react';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { useEffect } from 'react';
import { useNavigate } from 'react-router';
interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    const navigate = useNavigate();
    useEffect(() => {
        const auth = localStorage.getItem("user");
        if (auth) {
            navigate('/')
        }
    })

    return (
        <div className="Navbar">
            <Navbar expand="lg">
                <Container>
                    <Navbar.Brand href="/home">Dummy-project</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link href="/Features">Features</Nav.Link>
                            <Nav.Link href="/Pricing">Pricing</Nav.Link>
                            <Nav.Link href="/Blog">Blog</Nav.Link>
                            <Nav.Link href="/Examples">Examples</Nav.Link>
                            <Nav.Link href="/About">About</Nav.Link>
                            <Nav.Link href="/signUp">Sign Up</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
    );
};

export default App;
