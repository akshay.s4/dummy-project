import * as React from 'react';
import chess from '../../../Assets/Images/chess.jpg'
interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    return (
        <div className="features-container">
            <div className="features-inner main">
                <div className="feature-image">
                    <img src={chess} alt="img" className="img-chess" />
                </div>
                <div className="feature-text">
                    <div className="text-head">
                        <h3 className="fontFamily-Reckless text-l">Join more than 100,000 creators who’ve <span className="text-l fontFamily-Basis">sold over $1 billion in courses and coaching.</span></h3>
                        <p ><a href="https://teachable.com/features" className="fontFamily-Basis text-reg ">View Features</a></p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default App;