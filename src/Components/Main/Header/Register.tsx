import * as React from 'react';
import register from '../../../Assets/Images/register.jpg'
interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    return (
        <div className="Register-container ">
            <div className="register-inner">
                <div className="register-text">
                    <h1 className="fontFamily-Basis text-xl">Establish Your Business's Hub on Teachable</h1>
                    <p className="fontFamily-Basis text-reg">In this free mini course, learn how to showcase your business and start collecting leads on Teachable in just five days.</p>
                    <p><a href="https://teachable.com/features" className="fontFamily-Basis text-reg">Register for free</a></p>
                </div>
            </div>
            <div className="register-image">
                <img src={register} alt="img" className="img-r" />
            </div>
        </div>
    );
};

export default App;