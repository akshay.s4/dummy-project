import * as React from 'react';
import Button from 'react-bootstrap/Button';
interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    return (
        <div className="Signup-container">
            <div className="signup-content">
                <h2 className="fontFamily-Reckless text-xxl">Share what you know <br /> <span className="fontFamily-Basis  text-xxl">Sign up free today.</span></h2>
            </div>
            <div className="mb-3 " >
                <Button variant="secondary" style={{ background: "Black" }} size="lg">
                    Get started
                </Button>
            </div>
        </div>
    );
};

export default App;