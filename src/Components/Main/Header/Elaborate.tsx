import * as React from 'react';
interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    return (
        <div className="Elaborate">
            <div className="ElaborateContainer">
                <h1 className="fontFamily-Reckless text-x ">
                    The master of hand lettering. The Aristotle of coding. The authority on drone flying. From online courses to coaching and beyond, this is where creative entrepreneurs build the future.</h1>
            </div>
        </div>
    );
};

export default App;