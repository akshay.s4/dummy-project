import * as React from 'react';
import Carousel from 'react-bootstrap/Carousel';
import slide1 from '../../../Assets/Images/slide1.jpg'
import slide2 from '../../../Assets/Images/slide2.jpg'
import slide3 from '../../../Assets/Images/slide3.jpg'
interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    return (
        <Carousel fade interval={3000} controls={false}>
            <Carousel.Item>
                <div className="Review-Container">
                    <div className="review-inner">
                        <hr />
                        <div className="review-slide1">
                            <p className="fontFamily-Basis text-s">In my own words</p>
                            <h5 className="fontFamily-Reckless text-xl">"Ninety-five percent of my income comes from online courses. They are literally the perfect product. They’re scalable, fun to make, and you help a lot of people. I don’t normally get obsessed with tech. But I am obsessed with Teachable."</h5>
                            <p className="fontFamily-Basis text-s">Mariah Coz : Marketing for Course Creators</p>
                        </div>
                    </div>
                    <div className="slide-image">
                        <img src={slide1} alt="slide 1" />
                    </div>
                </div>
            </Carousel.Item>
            <Carousel.Item>
                <div className="Review-Container">
                    <div className="review-inner">
                        <hr />
                        <div className="review-slide1">
                            <p className="fontFamily-Basis text-s">In my own words</p>
                            <h5 className="fontFamily-Reckless text-xl">“What really has clinched it for me, why I chose Teachable, and why I love it and will continue to do my online courses with Teachable, is the great support that they have.”
                            </h5>
                            <p className="fontFamily-Basis text-s">Julia Stoian : Live. Write. Thrive</p>
                        </div>
                    </div>
                    <div className="slide-image">
                        <img src={slide2} alt="slide 2" />
                    </div>
                </div>
            </Carousel.Item>
            <Carousel.Item>
                <div className="Review-Container">
                    <div className="review-inner">
                        <hr />
                        <div className="review-slide1">
                            <p className="fontFamily-Basis text-s">In my own words</p>
                            <h5 className="fontFamily-Reckless text-xl">"An online course model has given us a unique kind of flexibility...the marginal cost per student is quite small, allowing us to scale a lot quicker than if we were doing, say, in-person training, or some other kind of high-touch industry service."</h5>
                            <p className="fontFamily-Basis text-s">Alan Perlman : Drone Pilot Ground School</p>
                        </div>
                    </div>
                    <div className="slide-image">
                        <img src={slide3} alt="slide 3" />
                    </div>
                </div>
            </Carousel.Item>
        </Carousel>
    );
};

export default App;