import * as React from 'react';
interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    return (
        <div className="features-details">
            <div className="features-Details">
                <div className="card1">
                    <div className="card-col">
                        <h4 className="fontFamily-Reckless text-m">1</h4>

                        <h3 className="fontFamily-Basis text-m">Easy as it gets.</h3>
                        <p className="fontFamily-Basis text-s">In a matter of minutes, you’ll have access to everything you could ever need to create and sell online courses and coaching—our stress-free platform makes it easy—tech skills or no tech skills.</p>

                    </div>
                    <div className="card-col">
                        <h4 className="fontFamily-Reckless text-m">2</h4>

                        <h3 className="fontFamily-Basis text-m">Easy as it gets.</h3>
                        <p className="fontFamily-Basis text-s">In a matter of minutes, you’ll have access to everything you could ever need to create and sell online courses and coaching—our stress-free platform makes it easy—tech skills or no tech skills.</p>

                    </div>
                    <div className="card-col">
                        <h4 className="fontFamily-Reckless text-m">3</h4>

                        <h3 className="fontFamily-Basis text-m">Easy as it gets.</h3>
                        <p className="fontFamily-Basis text-s">In a matter of minutes, you’ll have access to everything you could ever need to create and sell online courses and coaching—our stress-free platform makes it easy—tech skills or no tech skills.</p>

                    </div>
                </div>
            </div>
        </div>
    );
};

export default App;