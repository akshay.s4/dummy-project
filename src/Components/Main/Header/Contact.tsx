import * as React from 'react';
import contact from '../../../Assets/Images/contact.jpg'
interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    return (
        <div className="Contact-container">
            <div className="contact-inner">
                <div className="contact-img">
                    <img src={contact} alt="img" className="img-c" />
                </div>
            </div>
            <div className="contact-text">
                <h1 className="fontFamily-Reckless text-l">Award-winning support for creators and businesses <span className="text-l fontFamily-Basis">of all sizes and subjects.</span></h1>
                <p><a href="asd" className="fontFamily-Basis text-reg ">Contact us</a></p>
            </div>
        </div>
    );
};

export default App;