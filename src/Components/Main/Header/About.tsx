import * as React from 'react';
import img1 from '../../../Assets/Images/front.jpg';
import Button from 'react-bootstrap/Button';
interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    return (
        <div className="container">
            <div className="main main1">
                <div className="img-container grid-item">
                    <img src={img1} alt="img" className="img-c" />
                </div>
                <div className="text-container">
                    <div className="text grid-item" >
                        <div className="text-head">
                            <h1 className="fontFamily-Reckless text-xxl">Share what you <span className="text-xxl fontFamily-Basis">know.™</span></h1>
                        </div>
                        <div className="text-para">
                            <p className="text-xxl fontFamily-Basis">Transform your knowledge into a thriving business with the best online course platform for creators everywhere.</p>
                        </div>
                    </div>
                    <div className="email grid-item">
                        <div className="email-input">
                            <input type="email" placeholder="Your email here" className="email-box" />
                        </div>
                        <div className="email-btn">
                            <Button variant="secondary" style={{ background: "Black" }} size="lg">
                                Get started for free
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default App;