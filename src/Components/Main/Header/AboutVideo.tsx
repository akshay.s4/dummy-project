import * as React from 'react';
import video from '../../../Assets/Images/video.jpeg'
import Button from 'react-bootstrap/Button';
interface IAppProps {
}

const App: React.FunctionComponent<IAppProps> = (props) => {
    return (
        <div className="video-container">
            <div className="video-inner">
                <div className="video-inner">
                    <img src={video} alt="img" className="img-v" />
                </div>
                <div className="video-text">
                    <div className="video-para">
                        <p className="fontFamily-Basis text-s">Create online courses, build a brand, scale your business.</p>
                    </div>
                    <div className="mb-2">
                        <Button variant="secondary" style={{ background: "Black" }} size="lg">
                            Get started for free
                        </Button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default App;