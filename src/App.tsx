import React from 'react';

import { Routes, Route } from "react-router-dom";
import Navbar from './Components/Main/Navbar/Navbar';
import Footer from './Components/Main/Footer/footer'
import Main from './Components/Main/Main'
import FeaturesPage from './Components/Features/FeaturesPage';
import SignUp from './Components/Register/SignUp';
import PrivateComponent from './Components/PrivateComponent'
import './App.css';
import './Assets/css/index.css';

function App() {
  return (
    <>
      <Navbar />
      <Routes>

        <Route element={<PrivateComponent />}>
          <Route path="/" element={<Main />} />
          <Route path="/Home" element={<Main />} />
          <Route path="/Features" element={<FeaturesPage />} />
        </Route>

        <Route path="/signUp" element={<SignUp />} />
      </Routes>
      <Footer />
    </>






  );
}

export default App;
